#pragma once

#include "components/textUserInterface.hpp"

namespace Components {

template <typename GameType>
void TextUserInterface<GameType>::Visualise(const GameType& game) const
{
//To be implemented
}

template <typename GameType>
void TextUserInterface<GameType>::AllowMove() const
{
    bool validMove{false};
    std::string move;
    while(!validMove)
    {
        std::cin >> move;
        mPlayer.ParseMove(move);   
    }
}

} //namespace Components
