#pragma once

#include "components/IPiece.hpp"
#include "components/board.hpp"

#include "framework/IPlayer.hpp"
#include "framework/dummyPlayer.hpp"

#include <iostream>
#include <string>

namespace Components {

template <typename GameType>
class TextUserInterface<GameType>
{
public:
    TextUserInterface()
    : mPlayer(Framework::DummyPlayer())
    {
    }
    
    void BindPlayer(Framework::IPlayer& player) { mPlayer = player; }
    void Visualise(const GameType& game) const;
    void SendMessage(const std::string& message) const { std::cout << message << std::endl; }
    void AllowMove() const;
private:
    Framework::IPlayer mPlayer; 
};

} //namespace Components
