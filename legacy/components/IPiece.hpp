#pragma once

#include "components/board.hpp"

#include <string>
#include <functional>

class IPiece
{
public:
    using namespace std::placeholders;
    Piece(Board& board, int posx, int posy, int type, int side)
        : mBoard(board), mPosx(posx), mPosy(posy), mType(type), mSide(side)
    {
    }
    bool setPos(int posx, int posy) { mPosx = posx; mPosy = posy; }
    int getType() const { return mType; }
    int getSide() const { return mSide; }
    virtual bool tryMove(int posx, int posy) =0;
private:
    Board mBoard;
    int mPosx;
    int mPosy;
    int mPieceType;
    int mSide;
};
