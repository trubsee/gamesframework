#pragma once
#include "components/IPiece.hpp"
#include "userInterface/IUserInterface.hpp"

#include <vector>

class Board
{
public:
    using BoardType = std::array<std::array<IPiece*, 8>, 8>;
    Board()
    {
    }
    Board(BoardType initState);
        : mBoardData(initState)
    {
    }
    void setBoard(BoardType board) { mBoardData = board; }
    const BoardType& getSnapshot() const { return mBoardData; }
    void addPiece(IPiece* piece, int posx, int posy) { mBoardData[posx][posy] = piece; }
    void removePiece(int posx, int posy) { mBoardData[posx][posy] = nullptr; }
    IPiece getPiece(int posx, int posy) const { return mBoardData[posx][posy]; }
private:
    BoardType mBoardData;
};
