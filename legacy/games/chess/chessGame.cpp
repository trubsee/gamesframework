#include "games/chess/chessGame.hpp"
#include "games/chess/constants.hpp"

#include <vector>
#include <string>
#include <algorithm>

ChessGame::ChessGame(std::unique_ptr<IUserInterface> userInterface, bool singlePlayer)
    : mSide(side), mUI(userInterface), mBoard()
{ 
    createBoard();  
}

void ChessGame::createBoard()
{
    std::array<IPiece*, 8> whiteBackline {
        new Rook(mBoard, 0, 0, ROOK, WHITE),
        new Knight(mBoard, 1, 0, KNIGHT, WHITE),
        new Bishop(mBoard, 2, 0, BISHOP, WHITE),
        new Queen(mBoard, 3, 0, QUEEN, WHITE),
        new King(mBoard, 4, 0, KING, WHITE),
        new Bishop(mBoard, 5, 0, BISHOP, WHITE),
        new Knight(mBoard, 6, 0, KNIGHT, WHITE),
        new Rook(mBoard, 7, 0, ROOK, WHITE)};
    std::array<IPiece*, 8> whitePawns;
    for(int x = 0; i < 8; ++i)
    {
        whitePawns[i] = new Pawn(mBoard, i, 0, PAWN, WHITE);
    }
    
    std::array<IPiece*, 8> blackBackline {
        new Rook(mBoard, 0, 7, ROOK, BLACK),
        new Knight(mBoard, 1, 7, KNIGHT, BLACK),
        new Bishop(mBoard, 2, 7, BISHOP, BLACK),
        new Queen(mBoard, 3, 7, QUEEN, BLACK),
        new King(mBoard, 4, 7, KING, BLACK),
        new Bishop(mBoard, 5, 7, BISHOP, BLACK),
        new Knight(mBoard, 6, 7, KNIGHT, BLACK),
        new Rook(mBoard, 7, 7, ROOK, BLACK)};
    std::array<IPiece*, 8> blackPawns;
    for(int x = 0; i < 8; ++i)
    {
        blackPawns[i] = new Pawn(mBoard, i, 7, PAWN, BLACK);
    }

    std::array<IPiece*, 8> emptyLine;
    emptyLine.fill(nullptr);

    BoardType startBoard& {
        whiteBackline, whitePawns, emptyLine, emptyLine, emptyLine, emptyLine, blackPawns, blackBackline };

    mBoard.setBoard(startBoard); 
}

bool ChessGame::input(const std::string& command)
{
    std::vector<std::string> output;
    Move& move = parseMove(command);
    if(!move.valid)
    {
        output.emplace("Move not recognised, please try again");
        mUI->update(output);
        mUI->promptMove();
    }
    IPiece* piece = mBoard.getPiece(move.oldx, move.oldy);
    
    
    if(piece == nullptr)
    {
        output.emplace("There is no piece at that square, please try again");
        mUI->update(output);
        mUI->promptMove();
    }
    else if(piece->getSide() != mSide)
    {
        output.emplace("Piece selected is not your colour, please try again");
        mUI->update(output);
        mUI->promptMove();
    }
    else if(pice->getType() != move.piece)
    {
        output.emplace("Selected piece does not match the piece on the board, please try again");
        mUI->update(output);
    }
    else(piece->tryMove(move.newx, move.newy))
    {
        mBoard.removePiece(move.oldx, move.oldy);
        mBoard.addPiece(move.newx, move.newy);
        output.emplace("Piece successfully moved");
        mUI->update(output);
        mUI->promptMove();
    }
    output.emplace("Move suggested is not possible, please try again");
    mUI->update(output);
    mUI->promptMove();
}

const Move& ChessGame::parseMove(const std::string& move)
{
    Move move;
    for(std::string::iterator it = move.begin(); it != move.end(); ++it)
    {
        if(*it == " ")
        {
            continue;
        }
        else if(move.piece == -1)
        {
            switch(*it)
            {
                case ("P" || "p"):
                    move.piece = PAWN;
                case ("N" || "n"):
                    move.piece = KNIGHT;
                case ("B" || "b"):
                    move.piece = BISHOP;
                case ("R" || "r"):
                    move.piece = ROOK;
                case ("Q" || "q"):
                    move.piece = QUEEN;
                case ("K" || "k"):
                    move.piece = KING;
                default:
                    return move;
            }
        }
        else if(move.oldx == -1)
        {
            switch(*it)
            {
                case("A" || "a"):
                    move.oldx = 0;
                case("B" || "b"):
                    move.oldx = 1;
                case("C" || "c"):
                    move.oldx = 2;
                case("D" || "d"):
                    move.oldx = 3;
                case("E" || "e"):
                    move.oldx = 4;
                case("F" || "f"):
                    move.oldx = 5;
                case("G" || "g"):
                    move.oldx = 6;
                case("H" || "h"):
                    move.oldx = 7;
                default:
                    return move;
            }
        }
        else if(move.oldy == -1)
        {
            switch(*it)
            {
                case("0"):
                    move.oldy = 0;
                case("1"):
                    move.oldy = 1;
                case("2"):
                    move.oldy = 2;
                case("3"):
                    move.oldy = 3;
                case("4"):
                    move.oldy = 4;
                case("5"):
                    move.oldy = 5;
                case("6"):
                    move.oldy = 6;
                case("7"):
                    move.oldy = 7;
                default:
                    return move;
            }
        }
        else if(move.newx == -1)
        {
            switch(*it)
            {
                case("A" || "a"):
                    move.newx = 0;
                case("B" || "b"):
                    move.newx = 1;
                case("C" || "c"):
                    move.newx = 2;
                case("D" || "d"):
                    move.newx = 3;
                case("E" || "e"):
                    move.newx = 4;
                case("F" || "f"):
                    move.newx = 5;
                case("G" || "g"):
                    move.newx = 6;
                case("H" || "h"):
                    move.newx = 7;
                default:
                    return move;
            }
        }
        else
        {
            switch(*it)
            {
                case("0"):
                    move.newy = 0;
                case("1"):
                    move.newy = 1;
                case("2"):
                    move.newy = 2;
                case("3"):
                    move.newy = 3;
                case("4"):
                    move.newy = 4;
                case("5"):
                    move.newy = 5;
                case("6"):
                    move.newy = 6;
                case("7"):
                    move.newy = 7;
                default:
                    return move;
            }
            move.valid = true;
        }
    }
    return move;
}
