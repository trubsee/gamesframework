#include "components/IPiece.hpp"

class King : public IPiece
{
public:
    virtual tryMove(int posx, int posy) override;
};

class Queen : public IPiece
{
public:
    virtual tryMove(int posx, int posy) override;
};

class Rook : public IPiece
{
public:
    virtual tryMove(int posx, int posy) override;
};

class Bishop : public IPiece
{
public:
    virtual tryMove(int posx, int posy) override;
};

class Knight : public IPiece
{
public:
    virtual tryMove(int posx, int posy) override;
};

class Pawn : public IPiece
{
public:
    virtual tryMove(int posx, int posy) override;
};
