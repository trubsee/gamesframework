constexpr int KING = 0;
constexpr int QUEEN = 1;
constexpr int ROOK = 2;
constexpr int BISHOP = 3;
constexpr int KNIGHT = 4;
constexpr int PAWN = 5;

constexpr int WHITE = 0;
constexpr int BLACK = 1;
