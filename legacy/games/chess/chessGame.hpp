#pragma once

#include "games/IGame.hpp"

#include "games/chess/constants.hpp"

#include "components/board.hpp"
#include "components/IPiece.hpp"

class ChessGame : public IGame
{
public:
    using BoardType = std::array<std::array<IPiece*, 8>, 8>;
    ChessGame(std::unique_ptr<IUserInterface> userInterface, int side);
    void createBoard();
    virtual bool input(const std::string& command) override;
    virtual void start() override { mUI->promptMove() };
    const BoardType& getBoard() const { return mBoard.getSnapshot(); }
private:
    const Move& parseMove(const std::string& command);
    struct Move
    {
        bool valid {false};
        int piece {-1};
        int oldx {-1};
        int oldy {-1};
        int newx {-1};
        int newy {-1};
    }
    Board mBoard;
};
