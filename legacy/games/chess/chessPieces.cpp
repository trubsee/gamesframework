#include "games/chess/chessPieces.hpp"
#include "games/chess/constants.hpp"

#include <cmath>

bool King::tryMove(int newx, int newy)
{
    if(newx == mPosx && newy == mPosy)
    {
        return false;
    }
    else if(std::abs(newx-mPosx) < 2 && std::abs(newy-mPosy) < 2)
    {
        setPos(newx, newy);
        return true;
    }
    return false;
}

bool Queen::tryMove(int posx, int posy)
{
    if(std::abs(newx-mPosx) < 8 && newy == mPosy || std::abs(newy-mPosy) < 8 && newx == mPosx || std::abs(newx-mPosx) == std::abs(newy-mPosy) && std::abs(newx-mPosx) < 8)
    {
        setPos(newx, newy);
        return true;
    }
    return false;
}

bool Rook::tryMove(int posx, int posy)
{
    if(std::abs(newx-mPosx) < 8 && newy == mPosy || std::abs(newy-mPosy) < 8 && newx == mPosx)
    {
        setPos(newx, newy);
        return true;
    }
    return false;
}

bool Bishop::tryMove(int posx, int posy)
{
    if(std::abs(newx-mPosx) == std::abs(newy-mPosy) && std::abs(newx-mPosx) < 8)
    {
        setPos(newx, newy);
        return true;
    }
    return false;
}

bool Knight::tryMove(int posx, int posy)
{
    if(std::abs(newx-mPosx) == 3 && std::abs(newx-mPosx) == 2 || std::abs(newy-mPosy) == 3 && std::abs(newx-mPosy) == 3)
    {
        setPos(newx, newy);
        return true;
    }
    return false;
}

bool Pawn::tryMove(int posx, int posy)
{
    if(getSide() == WHITE && newy-mPosx == 1 || getSide() == BLACK && mPosy-newy == 1)
    {
        setPos(newx, newy);
        return true;
    }
    return false;
}
