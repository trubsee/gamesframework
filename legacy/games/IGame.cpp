#include <IGame.hpp>

void IGame::attachUI(IUserInterface* newUI)
{
    mListeningUIs.emplace(newUI);
}

bool IGame::detachUI(IUserInterface* existingUI)
{
    for(std::vector<IUserInterface*>::iterator it = mListenngUIs.begin(); it != mListeningUIs.end(); ++it)
    {
        if(exisitingUI == *it)
        {
            mListeningUIs.erase(it);
            return true;
        }
    }
    return false;
}
