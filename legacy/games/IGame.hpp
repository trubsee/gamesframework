#pragma once

#include <components/board.hpp>
#include <components/IPiece.hpp>

#include <memory>

class IGame
{
/// Interface for Game Objects
/// UIs should talk to game objects via this interface
/// Should be implemented as a factory that creates it's components
public:
    virtual IGame(std::unique_ptr<IUserInterface> userInterface, bool side)
        : mUI(userInterface), mSinglePlayer(singlePlayer)
    {
    }
    virtual void input(const std::string& move) = 0;
    virtual void start() = 0;
private:
    int mSide;
    std::unique_ptr<IUserInterface> mUI;
};
