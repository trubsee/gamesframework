#include <testInterface.hpp>

void TextInterface::update(const std::vector<std::string>& output)
{
    for(std::vector<std::string>& it = output.begin(); it != output.end(); ++it)
    {
        std::cout << *it << std::endl;
    }
}

void TextInterface::promptMove(std::string& move)
{
    std::cout << "Please enter your move: ";
    std::cin >> move;
    while (mGame.Input(move))
    {
        std::cout << "Please enter your move: ";
        std::cin >> move;
    }
}
