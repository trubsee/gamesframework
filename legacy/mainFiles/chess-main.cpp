#include "games/IGame.hpp"
#include "userInterface/IUserInterface.hpp"
#include "games/chess/constants.hpp"

int main()
{
    IUserInterface* playerOne = new TextInterface();
    IGame* chessGame = new ChessGame(playerOne, WHITE);
    chessGame.start(); 
    return 0;
}
