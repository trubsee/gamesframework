#include "games/chess/chessPiece.hpp"

namespace Games { namespace Chess {

using namespace Framework;

const std::vector<Move>& King::GetMoves(
    const Board& board,
    int oldx,
    int oldy)
{
    std::vector<Move> moves;
    const std::vector<Direction> directions { 
        Direction::N, 
        Direction::NE, 
        Direction::E, 
        Direction::SE, 
        Direction::S, 
        Direction::SW, 
        Direction::W, 
        Direction::NW};
    
    for(const auto direction : directions)
    { 
        Move newMove{oldx, oldy};
        newMove.MakeMove(direction);
        
        square = board.GetPiece(newMove.posx, newMove.posy);
        if(square != nullptr && square.GetSide() == mSide):
            continue;
        else if(board.limitCheck(newMove.posx, newMove.posy)):
            continue;
        else:
            moves.emplace_back(newMove);
    }
    return moves;
}

const std::vector<Move>& Queen::GetMoves(
    const Board& board,
    int oldx,
    int oldy)
{
    std::vector<Move> moves;
    const std::vector<Direction> directions { 
        Direction::N, 
        Direction::NE, 
        Direction::E, 
        Direction::SE, 
        Direction::S, 
        Direction::SW, 
        Direction::W, 
        Direction::NW};
    
    for(const auto direction : directions)
    { 
        bool isObstructed = false;
        Move newMove{oldx, oldy};
        
        while (!isObstructed):
        {
            newMove.MakeMove(direction);
        
            square = board.GetPiece(newMove.posx, newMove.posy);
            if(square != nullptr)
                isObstructed = true;
                if(square.GetSide() == mSide):
                    continue;
                else if(board.limitCheck(newMove.posx, newMove.posy)):
                    isObstructed = true;
                else:
                    moves.emplace_back(newMove);
        }
    }
    return moves;
}

const std::vector<Move>& Rook::GetMoves(
    const Board& board,
    int oldx,
    int oldy)
{
    std::vector<Move> moves;
    const std::vector<Direction> directions { 
        Direction::N, 
        Direction::E, 
        Direction::S, 
        Direction::W};
 
    for(const auto direction : directions)
    { 
        bool isObstructed = false;
        Move newMove{oldx, oldy};
        
        while (!isObstructed):
        {
            newMove.MakeMove(direction);
        
            square = board.GetPiece(newMove.posx, newMove.posy);
            if(square != nullptr)
                isObstructed = true;
                if(square.GetSide() == mSide):
                    continue;
                else if(board.limitCheck(newMove.posx, newMove.posy)):
                    isObstructed = true;
                else:
                    moves.emplace_back(newMove);
        }
    }
    return moves;
}

const std::vector<Move>& Bishop::GetMoves(
    const Board& board,
    int oldx,
    int oldy)
{
    std::vector<Move> moves;
    const std::vector<Direction> directions { 
        Direction::NE, 
        Direction::SE, 
        Direction::SW, 
        Direction::NW};
    
    for(const auto direction : directions)
    { 
        bool isObstructed = false;
        Move newMove{oldx, oldy};
        
        while (!isObstructed):
        {
            newMove.MakeMove(direction);
        
            square = board.GetPiece(newMove.posx, newMove.posy);
            if(square != nullptr)
            {
                isObstructed = true;
                if(square.GetSide() == mSide):
                    continue;
                else if(board.limitCheck(newMove.posx, newMove.posy)):
                    isObstructed = true;
                else
                    moves.emplace_back(newMove);
            }
        }
    }
    return moves;
}

const std::vector<Move>& Knight::GetMoves(
    const Board& board,
    int oldx,
    int oldy)
{
    std::vector<Move> moves;

    // FIXME: posx+1, posy + 2 ???
    //
    const std::vector<Move> movesBeforeLimitCheck { 
        Move { posx+3, posy+2},
        Move { posx+3, posy-2},
        Move { posx+2, posy+3},
        Move { posx+2, posy-3},
        Move { posx-3, posy+2},
        Move { posx-3, posy-2},
        Move { posx-2, posy+3},
        Move { posx-2, posy-3}}; 
    
    for(const auto& move : movesBeforeLimitCheck)
    { 
        if(!board.limitCheck(move.posx, move.posy)):
            moves.emplace_back(move);
    }
    return moves;
}

const std::vector<Move>& Pawn::GetMoves(
    const Board& board,
    int oldx,
    int oldy)
{
    std::vector<Move> moves;
    Move newMove {oldx, oldy};
    
    if(mSide == Side::WHITE)
    {
        newMove.MakeMove(Direction::N);
        square = board.GetPiece(newMove.posx, newMove.posy);
        if(!board.limitCheck(newMove.posx, newMove.posy) && square == nullptr):
            moves.emplace_back(newMove);
        
        if(oldy == 1)
        {
            newMove.MakeMove(Direction::N);
            square = board.GetPiece(newMove.posx, newMove.posy);
            if(!board.limitCheck(newMove.posx, newMove.posy) && square == nullptr):
                moves.emplace_back(newMove);
        }
        else if(oldy == 7)
        {
        //implement me
        }
    
    Move newMove {oldx, oldy};
    newMove.MakeMove(Direction::NE);
    square = board.GetPiece(newMove.posx, newMove.posy);
    if(!board.limitCheck(newMove.posx, newMove.posy) && square != nullptr && square.GetSide() != mSide):
        moves.emplace_back(newMove);
    
    Move newMove {oldx, oldy};
    newMove.MakeMove(Direction::NW);
    square = board.GetPiece(newMove.posx, newMove.posy);
    if(!board.limitCheck(newMove.posx, newMove.posy) && square != nullptr && square.GetSide() != mSide):
        moves.emplace_back(newMove);
    }
    
    else if(mSide == Side::BLACK)
    {
        newMove.MakeMove(Direction::S);
        square = board.GetPiece(newMove.posx, newMove.posy);
        if(!board.limitCheck(newMove.posx, newMove.posy) && square == nullptr):
            moves.emplace_back(newMove);
        
        if(oldy == 6)
        {
            newMove.MakeMove(Direction::S);
            square = board.GetPiece(newMove.posx, newMove.posy);
            if(!board.limitCheck(newMove.posx, newMove.posy) && square == nullptr):
                moves.emplace_back(newMove);
        }
        else if(oldy == 0)
        {
        //implement me
        }
    
    Move newMove {oldx, oldy};
    newMove.MakeMove(Direction::SE);
    square = board.GetPiece(newMove.posx, newMove.posy);
    if(!board.limitCheck(newMove.posx, newMove.posy) && square != nullptr && square.GetSide() != mSide):
        moves.emplace_back(newMove);
    
    Move newMove {oldx, oldy};
    newMove.MakeMove(Direction::SW);
    square = board.GetPiece(newMove.posx, newMove.posy);
    if(!board.limitCheck(newMove.posx, newMove.posy) && square != nullptr && square.GetSide() != mSide):
        moves.emplace_back(newMove);
    }
    return moves; 
}

}} //namespace Chess } namespace Games }
