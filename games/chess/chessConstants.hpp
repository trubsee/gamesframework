#pragma once

namespace Games { namespace Chess {

enum Side : char { WHITE, BLACK };
enum PieceId : char
{
KING = 'K',
QUEEN = 'Q',
ROOK = 'R',
BISHOP = 'B',
KNIGHT = 'H',
PAWN = 'P'
};

}} //namespace Chess } namesapce Games }
