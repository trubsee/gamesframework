#pragma once

#include "games/chess/chessConstants.hpp"

#include "framework/basePiece.hpp"

namespace Games { namespace Chess {

class King : public Framework::BasePiece
{
public:
    King(const Side side)
        : Framework::BasePiece(side, PieceId::KING)
    {
    }
 
    virtual const std::vector<Framework::Move>& GetMoves(
        const Framework::Board& board,
       int oldx,
        int oldy) const override;
};

class Queen : public Framework::BasePiece
{
public:
    Queen(const Side side)
        : Framework::BasePiece(side, PieceId::QUEEN)
    {
    }
    virtual const std::vector<Framework::Move>& GetMoves(
        const Frameworkd::Board& board,
        int oldx,
        int oldy) const override;
};

class Rook : public Framework::BasePiece
{
public:
    Rook(const Side side)
        : Framework::BasePiece(side, PieceId::ROOK)
    {
    }
    virtual const std::vector<Framework::Move>& GetMoves(
        const Framework::Board& board,
        int oldx,
        int oldy) const override;
};

class Bishop : public Framework::BasePiece
{
public:
    Bishop(const Side side)
        : Framework::BasePiece(side, PieceId::BISHOP)
    {
    }
    virtual const std::vector<Framework::Move>& GetMoves(
        const Framework::Board& board,
        int oldx,
        int oldy) const override;
};

class Knight : public Framework::BasePiece
{
public:
    Knight(const Side side)
        : Framework::BasePiece(side, PieceId::KNIGHT)
    {
    }
    virtual const std::vector<Framework::Move>& GetMoves(
        const Framework::Board& board,
        int oldx,
        int oldy) const override;
};

class Pawn : public Framework::BasePiece
{
public:
    Pawn(const Side side)
        : Framework::BasePiece(side, PieceId::PAWN)
    {
    }
    virtual const std::vector<Framework::Move>& GetMoves(
        const Framework::Board& board,
        int oldx,
        int oldy) const override;
};

