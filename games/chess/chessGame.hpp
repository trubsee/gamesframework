#pragma once

#include "framework/board.hpp"
#include "framework/IGameObject.hpp"
#include "framework/gameController.hpp"

namespace Games { namespace Chess {

template <typename UserInterface>
class ChessGame : public Framework::IGameObject<UserInterface>
{
public:
    ChessGame(UserInterface& userInterface);
        : mUserInterface(std::move(userInterface)), mBoard()
    {
    }
 
    virtual void TryMove(const std::string& move) override;
    virtual void Setup() override;
    virtual void VisualiseText override; 

private:
    Board<8,8> mBoard;
    UserInterface mUserInterface;
};

template <typename UserInterface>
class ChessHost : public ChessGame<UserInterface>, Framework::GameController<ChessGame>
{
public:    
    ChessHost(UserInterface& userInterface)
    : ChessGame<UserInterface>(std::move(userInterface))
    {
    }
};

}} //namespace Chess } namespac Games }
