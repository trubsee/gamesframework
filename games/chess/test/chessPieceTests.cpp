#include "framework/board.hpp"
#include "framework/move.hpp"

#include "games/chess/chessConstants.hpp"
#include "games/chess/chessPiece.hpp"

#include <boost/test/unit_test.hpp>

#include <algorithm>
#include <vector>

namespace Games { namespace Chess { namespace Test {

struct PieceFixture
{
    using ChessBoard = Framework::Board<8,8>;

    PieceFixture()
    :
        mBoard(ChessBoard::BoardType{}),
        mKing(Side::WHITE),
        mQueen(Side::WHITE),
        mRook(Side::WHITE),
        mBishop(Side::WHITE),
        mKnight(Side::WHITE),
        mWhitePawn(Side::WHITE),
        mBlackPawn(Side::BLACK)
    {
        mBoard.SetPiece(Bishop(Side::WHITE), 4, 5); // Be6
        mBoard.SetPiece(King(Side::WHITE),   5, 1); // Kf2
        mBoard.SetPiece(Knight(Side::WHITE), 2, 5); // Nc6
        mBoard.SetPiece(Queen(Side::WHITE),  3, 2); // Qd3
        mBoard.SetPiece(Rook(Side::WHITE),   6, 4); // Rg5
        mBoard.SetPiece(WhitePawn(Side::WHITE), 1, 1); // b2
        mBoard.SetPiece(BlackPawn(Side::BLACK), 1, 6); // b7

        mBishop = mBoard.GetPiece(4, 5);
        mKing = mBoard.GetPiece(5, 1);
        mKnight = mBoard.GetPiece(2, 5);
        mQueen = mBoard.GetPiece(3, 2);
        mRook = mBoard.GetPiece(6, 3);
        mWhitePawn = mBoard.GetPiece(1, 1);
        mBlackPawn = mBoard.GetPiece(1, 6);
    }

    ChessBoard mBoard;

    King& mKing;
    Queen& mQueen;
    Rook& mRook;
    Bishop& mBishop;
    Knight& mKnight;
    Pawn& mWhitePawn;
    Pawn& mBlackPawn;
};

}}} // namespace Games { namespace Chess { namespace Test {

BOOST_FIXTURE_TEST_SUITE(GamesChessPieceTest, Games::Chess::Test::PieceFixture)

BOOST_AUTO_TEST_CASE(KingPieceTest)
{
    using Move = Framework::Move;
    auto kingMoves = mKing.GetMoves(5, 1); // Kf2

    const std::vector<Move> moves{
        Move{5, 2}, // f3
        Move{6, 2}, // g3
        Move{6, 1}, // g2
        Move{6, 0}, // g1
        Move{5, 0}, // f1
        Move{4, 0}, // e1
        Move{4, 1}, // e2
        Move{4, 2}  // e3
    };

    BOOST_REQUIRE_EQUAL(kingMoves.size(), moves.size());
    BOOST_CHECK(std::is_permutation(moves, kingMoves));
}

BOOST_AUTO_TEST_CASE(QueenPieceTest)
{
    using Move = Framework::Move;
    auto queenMoves = mQueen.GetMoves(3, 2); // Qd3

    const std::vector<Move> moves{
        Move{3, 3}, // d4
        Move{3, 4}, // d5
        Move{3, 5}, // d6
        Move{3, 6}, // d7
        Move{3, 7}, // d8
        Move{4, 3}, // e4
        Move{5, 4}, // f5
        Move{6, 5}, // g6
        Move{7, 6}, // h7
        Move{4, 2}, // e3
        Move{5, 2}, // f3
        Move{6, 2}, // g3
        Move{7, 2}, // h3
        Move{4, 1}, // e2
        Move{5, 0}, // f1
        Move{3, 1}, // d2
        Move{3, 0}, // d1
        Move{2, 1}, // c2
        Move{1, 0}, // b1
        Move{2, 2}, // c3
        Move{1, 2}, // b3
        Move{0, 2}, // a3
        Move{2, 3}, // c4
        Move{1, 4}, // b5
        Move{0, 5}  // a6
    };

    BOOST_REQUIRE_EQUAL(queenMoves.size(), moves.size());
    BOOST_CHECK(std::is_permutation(moves, queenMoves));
}

BOOST_AUTO_TEST_CASE(RookPieceTest)
{
    using Move = Framework::Move;
    auto rookMoves = mRook.GetMoves(3, 2); // Rg4

    const std::vector<Move> moves{
        Move{6, 4}, // g5
        Move{6, 5}, // g6
        Move{6, 6}, // g7
        Move{6, 7}, // g8
        Move{7, 3}, // h4
        Move{6, 2}, // g3
        Move{6, 1}, // g2
        Move{6, 0}, // g1
        Move{5, 3}, // f4
        Move{4, 3}, // e4
        Move{3, 3}, // d4
        Move{2, 3}, // c4
        Move{1, 3}, // b4
        Move{0, 3}, // a4
    };

    BOOST_REQUIRE_EQUAL(rookMoves.size(), moves.size());
    BOOST_CHECK(std::is_permutation(moves, rookMoves));
}
BOOST_AUTO_TEST_CASE(BishopPieceTest)
{
    using Move = Framework::Move;
    auto bishopMoves = mBishop.GetMoves(4, 5); // Be6

    const std::vector<Move> moves{ 
        Move{5,6}, // f7
        Move{6,7}, // g8
        Move{5,4}, // f5
        Move{6,3}, // g4
        Move{7,2}, // h3
        Move{3,4}, // d5
        Move{2,3}, // c4
        Move{1,2}, // b3
        Move{0,1}, // a2
        Move{3,6}, // d7
        Move{2,7}  // c7
    };

    BOOST_REQUIRE_EQUAL(bishopMoves.size(), moves.size());

    BOOST_CHECK(std::is_permutation(moves, bishopMoves));
}

BOOST_AUTO_TEST_CASE(KnightPieceTest)
{
    using Move = Framework::Move;
    auto knightMoves = mKnight.GetMoves(2, 5); // Nc6

    const std::vector<Move> moves{
        Move{3, 7}, // d8
        Move{4, 6}, // e7
        Move{4, 4}, // e5
        Move{3, 3}, // d4
        Move{1, 3}, // b4
        Move{0, 4}, // a5
        Move{0, 6}, // a7
        Move{1, 7}  // b8
    };

    BOOST_REQUIRE_EQUAL(knightMoves.size(), moves.size());
    BOOST_CHECK(std::is_permutation(moves, knightMoves));
}

BOOST_AUTO_TEST_CASE(PawnPieceTest)
{
    using Move = Framework::Move;
}


BOOST_AUTO_TEST_SUITE_END()

