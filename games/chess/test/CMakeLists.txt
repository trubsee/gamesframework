cmake_minimum_required (VERSION 2.6)
project(gamesChessTest)

add_executable(gamesChessTest
    chessPieceTests.cpp
)

target_link_libraries(
    gamesChessTest

# FIXME
# add libraries later

    ${BOOST_UNIT_TEST_FRAMEWORK_LIBRARY_STATIC}
)
