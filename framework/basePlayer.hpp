#pragma once

#include "framework/IPlayer.hpp"

namespace Framework {

template <typename GameType, UserInterface>
class BasePlayer<GameType, UserInterface> : public IPlayer<GameType>
//  BasePlayer class provides generic implementation to
//  communicate with UserInterfaces. Derived classes should
//  overload ParseMove to provide specific implementation
{
public:
    BasePlayer(UserInterface& userInterface)
    : mUserInterface(userInterface)
    {
    }

    virtual void Update(GameType& game) const override { mUserInterface.Visualise(game); } 
    virtual void PromptMove() override { mUserInterface.AllowMove(); }

private:
    // implement player side state
    UserInterface mUserInterface; 
};

template <typename GameType, UserInterface>
class TogglePlayer<GameType, UserInterface> : public BasePlayer<GameType, UserInterface>
{
    virtual void Update(GameType& game) const override
    { 
        if(isActive)
            BasePlayer<GameType, UserInterface>::PromptMove();
        isActive = false;
    }
    
    virtual void PromptMove() override
    { 
        isActive = true; 
        BasePlayer<GameType, UserInterface>::PromptMove();
    }
private:
    mutable bool isActive{false};
};

} //namespace Framework
