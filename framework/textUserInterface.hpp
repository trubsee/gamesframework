#pragma once

#include "framework/board.hpp"
#include "framework/dummyPlayer.hpp"
#include "framework/IPiece.hpp"
#include "framework/IPlayer.hpp"

#include <iostream>
#include <string>

namespace Framework {

template <typename GameType>
class TextUserInterface<GameType>
{
public:
    void BindGame(const GameType& game) { mGame = game; }
    void Visualise() { mGame.VisualiseText(); }
    void SendMessage(const std::string& message) const { std::cout << message << std::endl; }
    void AllowMove() const;

private:
    GameType mGame;    
};

} //namespace Framework
