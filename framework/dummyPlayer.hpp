#pragma once

#include "IPlayer.hpp"

namespace Framework {

template <typename UserInterface>
class DummyPlayer : public IPlayer
{
public:
    virtual void Update(GameType& game) { mUserInterface.Visualise(game); }
    virtual void PromptMove();
    virtual bool ParseMove(const std::string& move);
};

template <typename UserInterface>
void DummyPlayer<UserInterface>::PromptMove() 
{ 
    mUserInterface.SendMessage("Player has been prompted to move"); 
}

template <typename UserInterface>
bool ParseMove(const std::string& move)
{
    mUserInterface.SendMessage("Unable to submit move, please bind a valid Player Class");
    return false;
}

} //namespace Framework

