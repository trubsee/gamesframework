#pragma once

#include <string>

namespace Framework {

template <typename GameType>
class IPlayer<GameType>
//  Interface that couples GameObjects with UserInterfaces
{
public:
    virtual ~IPlayer() = default;
    virtual void Update(GameType& game) const = 0;
    virtual void PromptMove() const = 0;
    virtual bool SubmitMove(const std::string& move) = 0;
};

} //namespace Framework
