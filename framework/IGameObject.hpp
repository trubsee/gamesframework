#pragma once

namespace Framework {

template <typename UserInterface>
class IGameObject
{
public:
    virtual ~IGameObject = default;
    virtual bool TryMove(const std::string& move) = 0;
    virtual void Setup() = 0; 
    virtual void VisualiseText() const = 0;
};

} //namespace Framework }
