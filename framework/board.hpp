#pragma once

#include "framework/IPiece.hpp"

#include <vector>
#include <memory>

namespace Framework {

template <std::size_t width, std::size_t height>
class Board<width, height>
{
public:
    using BoardType = std::array<std::array<std::unique_ptr<IPiece>, width>, height>;
    Board()
        : mBoardData((nullptr))
    {
    }

    Board(BoardType& initState)
        : mBoardData(std::move(initState))
    {
    }

    void SetBoard(BoardType& board) { mBoardData = std::move(board); }
    const BoardType& GetSnapshot() const { return mBoardData; }
    void SetPiece(std::unique_ptr<IPiece> piece, const int posx, const int posy) { mBoardData[posx][posy] = std::move(piece); }
    void RemovePiece(const int posx, const int posy) { mBoardData[posx][posy] = nullptr; }
    IPiece& GetPiece(const int posx, const int posy) const { return *mBoardData[posx][posy]; }
    const bool LimitCheck(const int posx, const int posy) const { return (posx < width && posy < height); }

private:
    BoardType mBoardData;
};

} //namespace Framework
