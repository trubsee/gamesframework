#pragma once

#include "framework/textUserInterface.hpp"

namespace Framework {

template <typename GameType>
void TextUserInterface<GameType>::AllowMove() const
{
    bool validMove{false};
    std::string move;
    while(!validMove)
    {
        std::cout << "Please make a move: "
        std::cin >> move;
        validMove = mGame.TryMove(move);   
    }
}

} //namespace Framework }
