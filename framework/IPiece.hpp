#pragma once

#include "framework/board.hpp"

#include <string>
#include <functional>
#include <vector>

namespace Framework {

enum Side : char;
enum PieceId : char;
struct Move;

class IPiece
{
public:
    virtual ~IPiece() = default;
    virtual const Side GetSide() const = 0;
    virtual const PieceId GetPieceId() const = 0;
    virtual const std::vector<Move>& GetMoves(
        const Board& board, 
        int posx, 
        int posy) const = 0;
};

} //namespace Framework }
