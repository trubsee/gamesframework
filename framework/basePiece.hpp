#pragma once

#include "framework/IPiece.hpp"
#include "framework/move.hpp"

namespace Framework {

class BasePiece : public IPiece
{
public:
    BasePiece(const Side side, const PieceId pieceId)
        : mSide(side), mPieceId(pieceId)
    {
    }
    
    virtual const Side GetSide() const override { return mSide; }
    virtual const PieceId GetPieceId() const override { return mPieceId; }

private:
    const Side mSide;
    const PieceId mPieceId;
};

} //namespace Framework }
