#pragma once
#include "GameController.hpp"

template <typename GameType>
void GameController::AddPlayer(std::unique_ptr<IPlayer> player)    
{
    if (std::find(mPlayers.begin(), mPlayers.end(), player) == mPlayers.end())
        mPlayers.emplace_back(std::move(player));
}

template <typename GameType>
void GameController::RemovePlayer(const IPlayer& player)
{
    for (const auto it = mPlayers.begin(); it != mPlayers.end(); ++it)
    {
        if (**it == player)
        {
            mPlayers.erase(it);
            break; 
        }
    }
    for (const auto it = mPlayerOrder.begin(); it != mPlayerOrder.end(); ++it)
    {
        if (*it == player)
            mPlayerOrder.erase(it); 
    }
}

template <typename GameType>
void GameController::Start()
{
    mGame.Setup();
    assert(!playerOrder.empty());
    mCurrentPlayer = mPlayerOrder.begin();
    mCurrentPlayer->PromptMove(); 
}

template <typename GameType>
void GameController::SubmitMove(GameType& game)
{
    mGame = game;
    if (++mCurrentPlayer == mPlayerOrder.end())
        mCurrentPlayer = mPlayerOrder.begin();
    mCurrentPlayer->PromptMove();
}

template <typename GameType>
void GameController::UpdatePlayers()
{
    for (const auto it = mPlayers.begin(); it != mPlayers.end(); ++it)
    {
        it->Update(*mGame); 
    }
}

