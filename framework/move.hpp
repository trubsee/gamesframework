#pragma once

namespace Framework {

enum Direction : int { N, NE, E, SE, S, SW, W, NW };
struct Move
{
    int posx;
    int posy;
    void MakeMove(const Direction direction)
    {
        switch(direction)
        {
            case N: ++posy;
            case NE: ++posx; ++posy;
            case E: ++posx;
            case SE: --posy; ++posx;
            case S: --posy;
            case SW: --posx; --posy;
            case W: --posx;
            case NW: ++posy; --posx;
        }
    }
};

} //namespace Framework
