#pragma once

#include "framework/IPlayer.hpp"

#include <memory>
#include <vector>
#include <algorithm>

namespace Framework {

template <typename GameType>
class GameController
//  Class that controls player order and updates the game state
{
    GameController(GameType& game)
    : mGame(game)
    {
    }

    void SetPlayerOrder(const std::vector<IPlayer>& playerOrder)
    {
        mPlayerOrder = playerOrder;
    }
    
    void AddPlayer(std::unique_ptr<IPlayer> player);
    void RemovePlayer(const IPlayer& player);
    void Start() const;

private:    
    void UpdatePlayers();

//  Implemented as a vector to allow for games where players leave
    std::vector<IPlayer> mPlayerOrder; 
    std::vector<IPlayer>::iterator mCurrentPlayer;
    std::vector<std::unique_ptr<IPlayer>> mPlayers;
    GameType mGame;
};

} // namespace Framework }
