#pragma once
#include "games/IGame.hpp"

class IUserInterface
{
/// Interface for UIs
/// listens to Game Objects and updates as required
/// allows for user input and parses to Game Object
public:
    IUserInterface()
    {
    }
    void connect(IGame* game) { mGame = game; }
    virtual void update(const std::vector<std::string>& output) = 0;
    virtual void promptMove() = 0; 
private:
    IGame* mGame;
};
