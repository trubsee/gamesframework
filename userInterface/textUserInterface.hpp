#pragma once

#include "userInterface/IUserInterface.hpp"

#include <vector>
#include <string>
#include <iostream>

class TextInterface : public IUserInterface
{
/// Basic text UI
public:    
    virtual void update(const std::vector<std::string>& output) override;
    virtual void promptMove(std::string move) override;
};

